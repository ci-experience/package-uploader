FROM alpine:latest

RUN apk add --no-cache curl git openssh-client

COPY srv_* /

RUN chmod +x /srv_*
