# Package Uploader
    Lightweight FTP/SFTP uploader for git projects.

## Scripts

* `/srv_git_version` - get current git tag
* `/srv_git_version_code` - get current amount of commits
* `/srv_git_full_version` - get full version

    **Warning:** GitLab CI variables are used!
* `/srv_put_to_sftp` - create directory (1st arg), upload file (2nd arg) to certain path (3rd arg)

    **Warning:** use `SFTP_*` variables and SSH keys!
* `/srv_upload_to_ftp` - upload file (1st arg) to certain path (2nd arg)

    **Warning:** use `FTP_*` variables!

## Usage

### Getting an image

#### Pull

Authenticate in registry (you must be a project member) and pull the image, then save it to your registry with `docker tag`/`docker push`.

OR

#### GitLab CI image

Use this image in your `.gitlab-ci.yml`.

**Note:** in order to get the image, use [DOCKER_AUTH_CONFIG](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#determine-your-docker_auth_config-data) variable.

OR (yes!)

#### Dockerfile build

Use this image as a base image in your Dockerfile to create your own one.

**Note:** do not forget to use `docker login` prior to that.

### Usage examples

Create a version file using the following example in `.gitlab-ci.yml`:

```
job:
  image: registry.gitlab.com/ci-experience/package-uploader:latest
  script:
    - /srv_git_full_version > version_file
```
Upload built package(s) to an FTP storage:
```
job:
  image: registry.gitlab.com/ci-experience/package-uploader:latest
  variables:
    FTP_HOST: ftp.example.com
    FTP_USER: dummy
  script:
    - /srv_upload_to_ftp package_file "projects/build/this_package_storage"
```
**Warning:** it is more safe to set all variables in the project's CI/CD settings!
